## pg redis docker express

pre req:

1.  docker
2.  node
3.  npm install -g nodemon

Steps

1. npm install
2. copy .env.expample to .env and set env vars if diff

3. start the pg db in docker

   docker compose up

4. create db in postres (connect with db ide of choice or ssh)

to connect ssh...

1.  `docker container list`
2.  get container id
3.  `docker exec -it 6579aeab9043 bash`
4.  from the bash prompt in the docker container
    `psql -U postgres`
5.  `\l` to list dbs
6.  `CREATE DATABASE items;`
7.  `\l` to list dbs
8.  `\c items` to change dbs to items
    9 exec the following

`CREATE TABLE items (id BIGSERIAL not null primary key, name VARCHAR(50) NOT NULL, description VARCHAR(500) NOT NULL, created_at TIMESTAMP DEFAULT now() );`

`Insert into items (name , description) values ('a thing', 'this is a thing');`

`CREATE TABLE users (id BIGSERIAL not null primary key, first_name VARCHAR(50), last_name VARCHAR(50), email VARCHAR(500), password VARCHAR(500),created_at TIMESTAMP DEFAULT now()) `

5. npm run dev to start app

open browser or insomnia or postman or thunder client or whatever floats your boat...
and hit localhost:3010/api/vi/items

there is put, post, and delete routes also get 1 item route
