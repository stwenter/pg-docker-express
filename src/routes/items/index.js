const express = require('express');
const {
  getItems,
  getOneItemById,
  insertOneItem,
  updateOneItem,
  deleteItemById,
} = require('../../controllers/itemController');

const db = require('../../db');
const router = express.Router({ mergeParams: true });

router.get('/items', getItems);
router.get('/items/:id', getOneItemById);
router.post('/items', insertOneItem);
router.put('/items/:id', updateOneItem);
router.delete('/items/:id', deleteItemById);

module.exports = router;
