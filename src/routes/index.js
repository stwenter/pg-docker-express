const express = require('express');

const router = express.Router({ mergeParams: true });

const auth = require('./auth');
const items = require('./items');

router.get('/', (req, res) => {
  res.status(200).send('OK');
});

router.use(auth);
router.use(items);

module.exports = router;
