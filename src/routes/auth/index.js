const express = require('express');

const router = express.Router({ mergeParams: true });

const {
  registerUser,
  logoutUser,
  authenticateUser,
  loginUser,
  refreshToken,
} = require('../../controllers/authController');

router.post('/register', registerUser);

router.delete('/logout', logoutUser);

router.post('/login', authenticateUser, loginUser);

router.post('/token', refreshToken);

module.exports = router;
