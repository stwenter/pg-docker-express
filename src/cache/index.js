const { createClient } = require('redis');

const client = createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
  password: process.env.REDIS_PASSWORD,
});

client.on('connect', () => {
  console.log('Redis client connected');
});

client.on('ready', () => {
  console.log('Redis client ready');
});

client.on('end', () => {
  console.log('Redis client disconnected');
});

client.on('error', (err) => {
  console.error(err.message);
});

process.on('SIGINT', () => {
  client.quit();
});

module.exports = client;
