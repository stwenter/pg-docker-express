require('dotenv').config();

const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');

const redisClient = require('./cache');

const routes = require('./routes');
const { authorizationRequired } = require('./controllers/authController');

const app = express();

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(cookieParser());

app.use(
  express.urlencoded({
    extended: true,
  }),
);

redisClient.connect();

// api routes
app.use('/api', routes);

app.get('/test', authorizationRequired, (req, res) => {
  res.status(200).json({ message: 'Authorized' });
});

module.exports = app;
