const db = require('../db');

module.exports.getItems = async (req, res) => {
  try {
    const { rows } = await db.query('SELECT * FROM items');
    res.status(200).json({
      status: 'sucess',
      results: rows.length,
      data: {
        items: rows,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(403).json({
      status: 'error',
      error: error.message,
    });
  }
};

module.exports.getOneItemById = async (req, res) => {
  try {
    const { id } = req.params;
    const { rows } = await db.query('SELECT * FROM items where id = $1', [id]);
    res.status(200).json({
      status: 'sucess',
      data: {
        item: rows[0],
      },
    });
  } catch (error) {
    console.log(error);
    res.status(403).json({
      status: 'error',
      error: error.message,
    });
  }
};

module.exports.insertOneItem = async (req, res) => {
  try {
    const { name, location, price_range } = req.body;

    const query = `INSERT INTO items (name, description) VALUES ($1, $2) RETURNING *`;
    const values = [name, location, price_range];
    const { rows } = await db.query(query, values);
    res.status(201).json({
      status: 'sucess',
      data: {
        items: rows,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(403).json({
      status: 'error',
      error: error.message,
    });
  }
};

module.exports.updateOneItem = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, location, price_range } = req.body;

    const query = `update items set name = $2, description = $3  where id = $1 RETURNING *`;
    const values = [id, name, description];
    const { rows } = await db.query(query, values);

    res.status(200).json({
      status: 'sucess',
      data: {
        item: rows[0],
      },
    });
  } catch (error) {
    console.log(error);
    res.status(403).json({
      status: 'error',
      error: error.message,
    });
  }
};

module.exports.deleteItemById = async (req, res) => {
  try {
    const { id } = req.params;
    const { rows } = await db.query('DELETE FROM items where id = $1', [id]);
    res.status(204).json({
      status: 'sucess',
      data: {
        item: rows[0],
      },
    });
  } catch (error) {
    console.log(errors);
    res.status(403).json({
      status: 'error',
      error: error.message,
    });
  }
};
