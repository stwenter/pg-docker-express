const { genSalt, hash } = require('bcryptjs');


module.exports = async function registerUser(email, password) {

    const { rows } = await db.query('SELECT email FROM users');

    if(rows)

  // gen salt
  const salt = await genSalt(10);
  // hash
  const hashedPassword = await hash(password, salt);
  // store in db
  const user = await register(email, hashedPassword, salt);

  return user;
};
